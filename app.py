import tkinter as tk
from autosaver import Autosaver
from tkinter.ttk import Frame, Label, Style
from Carbon.Aliases import true

class App(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
        
        homeView = Autosaver(self)
        homeView.show()

def main():

    root = tk.Tk()
    root.geometry("500x200")
    root.title("Autosaver")
    app = App(root)
    app.pack(side="top", fill="both", expand=True)
    root.mainloop()

if __name__ == '__main__':
    main()