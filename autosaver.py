import tkinter as tk
from repositories import Repositories
from tkinter.ttk import Frame, Label, Style

class View(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
    def show(self):
        self.lift()

class Autosaver(View):
    
    def __init__(self, *args, **kwargs):
        View.__init__(self, *args, **kwargs)
        self.initUI()


    def initUI(self):
        
        reposView = Repositories(self)

        self.pack(fill='both',expand=True)

        title = tk.Label(self, text="Autosaver", font="Helvetica 15 bold")
        title.pack()
        
        frame1 = tk.Frame(self)
        frame1.pack(fill='x')
        
        repoLabel= tk.Label(frame1, text="Selecteer repository", font="helvetica 12")
        repoLabel.place(x=20, y=20)
        repoLabel.pack(side="left", padx=5, pady=5)
        
        choices = ['testrepo1', 'testrepo2', 'testrep3']
        variable = tk.StringVar(frame1)
        variable.set('testrepo1')
        
        frame2 = tk.Frame(self, height=100)
        frame2.pack(fill='x')
        
        repoChoice = tk.OptionMenu(frame1, variable, *choices)
        repoChoice.pack(side="right", fill='x', padx=5, expand=True);
        #
        gitcommit = tk.Label(frame2, text="Git commit message", font="helvetica 12")
        gitcommit.pack(side = 'left', padx=5, pady=5)
        message = tk.Entry(frame2, bd=0, font="helvetica 12")
        message.place(x = 10,y = 10,width=200,height=100)
        message.config(highlightthickness=1, highlightbackground = "#ddd", highlightcolor= "#ddd")
        message.pack(side = 'right', fill='x', padx=5, expand=True)
        
        btnframe = tk.Frame(self, relief='raised')
        btnframe.pack(fill=tk.BOTH, expand=True)
        
        saveButton = tk.Button(btnframe, text="Opslaan")
        saveButton.pack(side="right", padx=5, pady=5)
        repButton = tk.Button(btnframe, text="Repositories", command=reposView.lift)
        repButton.pack(side="left")