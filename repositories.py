import tkinter as tk
from tkinter.ttk import Frame, Label, Style

class View(Frame):
    def __init__(self, *args, **kwargs):
        Frame.__init__(self, *args, **kwargs)
    def show(self):
        self.lift()

class Repositories(View):
    
    def __init__(self, *args, **kwargs):
        View.__init__(self, *args, **kwargs)
        self.initUI()


    def initUI(self):

        self.pack(fill='both',expand=True)

        title = tk.Label(self, text="Repositories", font="Helvetica 15 bold")
        title.pack()